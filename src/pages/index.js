import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from '../components/layout'
import Ordering from '../components/ordering'

class IndexPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sortby: 'order',
    }
  }

  render() {
    const planets = this.props.data.planets.edges.map(n => n.node.frontmatter)

    planets.sort((a, b) => {
      if (a[this.state.sortby] < b[this.state.sortby]) return -1
      else if (a[this.state.sortby] > b[this.state.sortby]) return 1
      else return 0
    })

    return (
      <Layout>
        <h2>all the planets</h2>
        <Ordering
          active={this.state.sortby}
          onChange={ordering => this.setState({ sortby: ordering })}
        />
        <ul>
          {planets.map(planet => (
            <li key={`planet-${planet.path}`}>
              {planet[this.state.sortby]}:{' '}
              <Link to={planet.path}>{planet.title}</Link>
            </li>
          ))}
        </ul>
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query {
    planets: allMarkdownRemark(
      sort: { order: ASC, fields: frontmatter___order }
    ) {
      edges {
        node {
          frontmatter {
            title
            path
            order
            radius
          }
        }
      }
    }
  }
`
